using Presentation.Models;

namespace DAL
{
    public interface IRepository
    {
        bool Register(User user);
    }
}