using System;
using System.Collections.Generic;
using System.Linq;
using Presentation.Models;

namespace DAL
{
    public class UserRepository : IRepository
    {
        private List<User> list;
        public UserRepository()
        {
            list = new List<User>();
        }

        public bool Register(User user)
        {
            try
            {
                list.Add(user);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }
}