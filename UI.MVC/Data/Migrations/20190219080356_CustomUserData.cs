﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UI.MVC.Data.Migrations
{
    public partial class CustomUserData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FavouriteDrink",
                table: "AspNetUsers",
                newName: "LicensePlate");

            migrationBuilder.RenameColumn(
                name: "FavouriteColor",
                table: "AspNetUsers",
                newName: "CarModel");

            migrationBuilder.AddColumn<string>(
                name: "CarBrand",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "CarHorsepower",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CarBrand",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "CarHorsepower",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "LicensePlate",
                table: "AspNetUsers",
                newName: "FavouriteDrink");

            migrationBuilder.RenameColumn(
                name: "CarModel",
                table: "AspNetUsers",
                newName: "FavouriteColor");
        }
    }
}
