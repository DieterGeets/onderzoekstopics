using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace UI.MVC.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        public string LicensePlate { get; set; }
        [Required]
        public string CarBrand { get; set; }
        [Required]
        public string CarModel { get; set; }
        [Required]
        public double CarHorsepower { get; set; }
        //public string CarImagePath { get; set; }
    }
}