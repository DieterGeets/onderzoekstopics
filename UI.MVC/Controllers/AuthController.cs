using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UI.MVC.Data;
using UI.MVC.Models;

namespace UI.MVC.Controllers
{

    public class AuthController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationDbContext _applicationDb;
        private readonly UserManager<ApplicationUser> _userManager;
        public AuthController(ApplicationDbContext appl, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> sign)
        {
            _signInManager = sign;
            _applicationDb = appl;
            _userManager = userManager;
        }
        
        public IActionResult Login()
        {
            return View();
        }

        public IActionResult SignIn()
        {
            var properties = _signInManager.ConfigureExternalAuthenticationProperties("Facebook", Url.Action("ExternalLoginCallback", "Auth"));
            return Challenge(properties, "Facebook");
        }   
        
        public async Task<IActionResult> ExternalLoginCallback()
        {
            //ExternalLoginInfo info = await _signInManager.GetExternalLoginInfoAsync();
            //info.Principal //the IPrincipal with the claims from facebook
            //info.ProviderKey //an unique identifier from Facebook for the user that just signed in
            //info.LoginProvider //a string with the external login provider name, in this case Facebook

            //to sign the user in if there's a local account associated to the login provider
            //var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false);            
            //result.Succeeded will be false if there's no local associated account 

            //to associate a local user account to an external login provider
            //await _userInManager.AddLoginAsync(aUserYoullHaveToCreate, info);        
            return Redirect("/");
        }

        public IActionResult Data()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddData(ApplicationUser a)
        {
            var user = _userManager.GetUserAsync(User);
            user.Result.CarBrand = a.CarBrand;
            user.Result.CarModel = a.CarModel;
            user.Result.LicensePlate = a.LicensePlate;
            user.Result.CarHorsepower = a.CarHorsepower;
            _applicationDb.SaveChanges();
            return RedirectToAction("Data");
        }
        
        
        
        
    }
}