using BL;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Presentation.Models;

namespace Presentation.Controllers
{
    public class RegisterController : Controller
    {
        private UserManager Manager = new UserManager();
        // GET
        [HttpGet]
        public IActionResult RegisterIndex()
        {
            return
            View();
        }
        
        [HttpPost]
        public IActionResult RegisterIndex(User user)
        {
            Manager.CreateUser(user);
            
            return
                RedirectToAction("Index", "Home");
        }
    }
}