using System.ComponentModel.DataAnnotations;

namespace Presentation.Models
{
    public class User
    {
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Username is mandatory")]
        public string Username { get; set; }
        
        [Required(ErrorMessage = "Password is mandatory")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
        [Required(ErrorMessage = "Iq is mandatory")]
        [Range(60,260, ErrorMessage = "Iq must be between 60 and 260")]
        public int Iq { get; set; }
    }
}