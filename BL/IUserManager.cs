using Presentation.Models;

namespace BL
{
    public interface IUserManager
    {
        bool CreateUser(User user);
    }
}