using DAL;
using Presentation.Models;

namespace BL
{
    public class UserManager : IUserManager
    {
        private UserRepository repo;
        public UserManager()
        {
            repo = new UserRepository();
        }


        public bool CreateUser(User user)
        {
            return repo.Register(user);
        }
    }
}